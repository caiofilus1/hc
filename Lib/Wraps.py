from functools import wraps

from marshmallow import fields
from marshmallow.fields import Nested
from marshmallow_enum import EnumField
from sqlalchemy import inspect

from Database import IntList
from Globals import marshmallow, db
from Database.Models import EnumSmart


class ModelSchema(marshmallow.ModelSchema):
    class Meta:
        sqla_session = db.session


def Schema(nested=None, declared_fields=[], exclude=[]):
    def decorator(cls):
        @wraps(cls)
        def wrapped(*args, **kwargs):
            class_attributes = dict()
            _nested = list(nested) if nested else []
            try:
                inspectedModel = inspect(cls)
                mapper = inspectedModel.relationships

            except:
                return cls

            for column in inspectedModel.columns.values():
                if isinstance(column.type, EnumSmart):
                    class_attributes[column.name] = EnumField(column.type._enum, by_value=column.type.by_value)
                elif isinstance(column.type, IntList):
                    class_attributes[column.name] = fields.List(fields.Integer())
                elif column.foreign_keys:
                    class_attributes[column.name] = fields.Integer()
            for item in _nested:
                relationship = mapper[item]
                relation_schema = relationship.argument.Schema
                class_attributes[item] = Nested(relation_schema, many=relationship.uselist)

            m = ModelSchema
            mc = type(m)
            MSchema = mc(cls.__name__ + 'MSchema', (m,), {
                'Meta': type('Meta', (), {'model': cls,
                                          'fields': declared_fields,
                                          'exclude': exclude, }),
                **class_attributes,
            })
            setattr(cls, 'Schema', MSchema)
            return cls

        return wrapped(cls)

    return decorator
