import time
from functools import wraps

import jwt
from flask import request, jsonify, globals, make_response
from marshmallow import ValidationError
from sqlalchemy.exc import InvalidRequestError, DisconnectionError, StatementError, TimeoutError
from typing import Type

from sqlalchemy.orm import make_transient
from werkzeug._compat import iterlists
from werkzeug.datastructures import MultiDict

import Globals
from Database.Models import UserInterface
from Globals import current_user, db

from Globals import marshmallow
from Error import Error
from Lib.Wraps import ModelSchema, Schema

form = None


class MyDict(MultiDict):
    def __init__(self, mapping):
        MultiDict.__init__(self, None)
        if isinstance(mapping, MultiDict):
            dict.__init__(self, ((k, l[:]) for k, l in iterlists(mapping)))
        elif isinstance(mapping, dict):
            tmp = {}
            self.__interDict(mapping, tmp)
            dict.__init__(self, tmp)
        else:
            tmp = {}
            for key in mapping:
                tmp.setdefault('data', []).append(key)
            dict.__init__(self, tmp)

    def __interDict(self, dic, result: dict, lastKey: str = '') -> None:
        if isinstance(dic, dict):
            for key, value in dic.items():
                if lastKey != '':
                    self.__interDict(value, result, lastKey + '-' + key)
                else:
                    self.__interDict(value, result, key)
        else:
            if isinstance(dic, list):
                for i in range(len(dic)):
                    self.__interDict(dic[i], result, lastKey + '-' + str(i))
            else:
                tu = {lastKey: [dic]}
                result.update(tu)
        return None


# def dictToStruct(schema, struct):
#     for item in schema:
#         if isinstance(schema, dict) or isinstance(schema, object):
#             dictToStruct(item, )


def validateInput(form_validator: Type[Schema]):
    def decorator(function):
        @wraps(function)
        def wrapper(*args, **kwargs):
            global form
            if request.method == 'GET':
                data = request.args
            else:
                if request.is_json:
                    data = request.json
                else:
                    data = request.form
            try:
                form = form_validator().load(data)
            except ValidationError as error:
                return Error.api_response(Error.FORM_ERROR, data=error.messages)
            return function(*args, **kwargs)

        return wrapper

    return decorator


def login_requeried(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        try:
            token = request.headers['token']
            # if request.is_json:
            #     json = request.json
            #     token = json['token']
            # else:
            #     token = request.args['token']
        except KeyError:
            return Error.api_response(Error.MISSING_TOKEM)
        try:
            data = jwt.decode(token, globals.current_app.config['SECRET_KEY'])
            if data['exp'] < time.time():
                return Error.api_response(Error.USER_NOT_LOGGED)
            try:
                user = UserInterface.query.filter_by(id=data['id']).first()
                make_transient(user)
                Globals.current_user = user
            except KeyError:
                return Error.api_response(Error.MISSING_VALUE)
            except Exception as e:
                print(str(e))
                return Error.api_response(Error.USER_NOT_FOUND)

        except Exception as e:
            print(str(e))
            return Error.api_response(Error.INVALID_TOKEN)
        return f(*args, **kwargs)

    return decorated


def admin_required(f):
    @wraps(f)
    @login_requeried
    def decorated(*args, **kwargs):
        if Globals.current_user['is_owner']:
            return f(*args, **kwargs)
        else:
            return Error.api_response(Error.NOT_A_ADMIN)

    return decorated


def found_atorization(cls, query, id):
    if cls == type(query):
        return True

    if type(query) == dict:
        for key, item in query:
            if found_atorization(cls, item):
                return True
    if type(query) == list:
        for item in query:
            if found_atorization(cls, item):
                return True
    return False


def base_getter(model: db.Model.__class__, schema: Type[marshmallow.ModelSchema] = None, seconderyFilter=None):
    def decorator(function):
        @wraps(function)
        def wrapper(*args, **kwargs):
            if not schema and not hasattr(model, 'Schema'):
                raise TypeError('You Need to set a DB Model with Schema or pass Schema')
            elif not schema and hasattr(model, 'Schema'):
                _schema = model.Schema
            else:
                _schema = schema
            query_filter = dict(request.args)
            query_filter.update(function(*args, **kwargs) or kwargs)
            if 'token' in query_filter:
                query_filter.pop('token')
            if hasattr(model, 'client_id'):
                query_filter['client_id'] = Globals.current_user['client_id']
            try:
                query = model.query.filter_by(**query_filter).all()
                # print(query[0].machine[0].sensor)
                if seconderyFilter:
                    seconderyFilter(query)
                if len(query) > 1:
                    try:
                        response = {model.__tablename__: _schema(many=True).dump(query).data}
                    except:
                        response = {model.__tablename__: _schema(many=True).dump(query)}
                elif len(query) == 1 and 'id' not in kwargs:
                    try:
                        response = {model.__tablename__: _schema(many=True).dump(query).data}
                    except:
                        response = {model.__tablename__: _schema(many=True).dump(query)}
                elif len(query) == 1:
                    response = {model.__tablename__: _schema().dump(query[0]).data}
                else:
                    response = {model.__tablename__: []}
                return Error.api_response(data=response)
            except DisconnectionError or TimeoutError:
                return Error.api_response(Error.SERVER_DISCONNECT)
            except InvalidRequestError or StatementError as e:
                print(e)
                return Error.api_response(Error.WRONG_REQUEST)
            except Exception as e:
                print(e)
                return Error.api_response(Error.INTERN_ERROR)

        return wrapper

    return decorator


def base_post(flask_form: Type[ModelSchema], model: Type[db.Model] = None, after_commit=None):
    def decorator(function):
        @wraps(function)
        @validateInput(flask_form)
        def wrapper(*args, **kwargs):
            try:
                global form
                if not model and not hasattr(flask_form, "__model__"):
                    _model = flask_form.__model__
                else:
                    _model = model

                struct = form
                kwargs['struct'] = struct
                ctx = function(*args, **kwargs)
                if isinstance(ctx, Error):
                    return Error.api_response(ctx)
                try:
                    db.session.add(struct)
                    db.session.commit()
                except Exception as e:
                    print(e)
                    db.session.rollback()
                    return Error.api_response(Error.INTERN_ERROR, msg=str(e))
                # se a minha função after_commit foi preenchida
                if after_commit:
                    ctx = after_commit(struct)
                    if isinstance(ctx, Error):
                        return Error.api_response(ctx)
                response = {'id': struct.id}
                return Error.api_response(data=response)
            except KeyError as e:
                return Error.api_response(Error.MISSING_VALUE, msg=str(e))

        return wrapper

    return decorator


def base_put(model: db.Model.__class__, input_validator: Type[ModelSchema], after_commit=None):
    def decorator(function):
        @wraps(function)
        @validateInput(input_validator)
        def wrapper(*args, **kwargs):
            try:
                global form
                struct = model.query.filter_by(id=str(kwargs['id'])).first()
                if not struct:
                    return Error.api_response(Error.ON_DB, msg=model.__tablename__ + " not found")

                kwargs['struct'] = struct
                kwargs.pop('id')
                struct.id = form.id
                function(*args, **kwargs)
                try:
                    db.session.commit()
                except Exception as e:
                    db.session.rollback()
                    return Error.api_response(Error.ON_DB, msg=str(e))
                if after_commit:
                    result = after_commit(struct)
                    return result if result else Error.api_response()
                return Error.api_response()
            except KeyError as e:
                return Error.api_response(Error.MISSING_VALUE, str(e))

        return wrapper

    return decorator


def base_delete(model: db.Model.__class__):
    def decorator(function):
        @wraps(function)
        def wrapper(*args, **kwargs):
            try:
                ctx = function(*args, **kwargs)
                if isinstance(ctx, Error):
                    return Error.api_response(ctx)
                query = {"id": kwargs['id']}
                if getattr(model, 'client_id', False):
                    query['client_id'] = Globals.current_user['client_id']
                try:
                    struct = model.query.filter_by(**query).first()
                    db.session.delete(struct)
                    db.session.commit()
                except Exception as e:
                    print(e)
                    return Error.api_response(Error.ON_DB, msg=model.__tablename__ + " não encontrado")
                return Error.api_response()
            except KeyError as e:
                print(e)
                return Error.api_response(Error.MISSING_VALUE)
            except Exception as e:
                print(e)
                return Error.api_response(Error.ON_DB)

        return wrapper

    return decorator

# def paginator(model: db.Model.__class__, schema: marshmallow.ModelSchema.__class__):
#     def decorator(function):
#         @wraps(function)
#         def wrapper(*args, **kwargs):
#             try:
#                 if kwargs['id'] != 0:
#                     return function(*args, **kwargs)
#             except KeyError:
#                 pass
#             page = request.args.get('page', default=1, type=int)
#             per_page = request.args.get('per_page', default=100, type=int)
#             sort = request.args.get('sort', default='id|asc', type=str).split('|')
#
#             query = model.query \
#                 .filter_by(client_id=current_user['client_id']) \
#                 .order_by(exec('model.' + sort[0] + '.' + sort[1] + '()')) \
#                 .paginate(page=page, per_page=per_page)
#             data = []
#             for user in query.items:
#                 aux = schema().dump(user).data
#                 data.append(aux)
#             query.items = data
#             return jsonify(PaginationSchema().dump(query).data)
#
#         return wrapper
#
#     return decorator


# def get_paginator(model: db.Model.__class__, schema: marshmallow.ModelSchema.__class__):
#     def decorator(function):
#         @wraps(function)
#         @paginator(model, schema)
#         @base_getter(model, schema)
#         def wrapper(*args, **kwargs):
#             return function(args, kwargs)
#
#         return wrapper
#
#     return decorator
