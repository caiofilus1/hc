import os
from datetime import datetime, date, timedelta
from time import time

import jwt
from flask import Blueprint, request, current_app
from flask_restful import Api, Resource
from marshmallow import ValidationError

import Globals
from Error import Error
from Form import LoginForm, UserForm, AppointmentForm, AdminForm, BloodBankForm
from Globals import db
from Database.Models import Appointment, Admin, UserInterface, AppointmentStatus, BloodBank
from Lib.ApiWrap import login_requeried, base_post, base_put, base_getter

api_bluePrint = Blueprint('api_blueprint', __name__,
                          url_prefix='/api')

api = Api(api_bluePrint)


@api.resource('/login')
class LoginAPI(Resource):
    def post(self):
        try:
            json = LoginForm().load(request.get_json())
        except ValidationError as err:
            return Error.api_response(Error.FORM_ERROR, msg=str(err.messages))

        user = UserInterface.query.filter_by(email=json['email']).first()
        if not user:
            return Error.api_response(Error.USER_NOT_FOUND)
        elif json['password'].startswith('pbkdf2'):

            if user.password != json['password']:
                return Error.api_response(Error.USER_NOT_FOUND)
        else:
            if not user.check_password(json['password']):
                return Error.api_response(Error.USER_NOT_FOUND)
        user = user.Schema().dump(user)
        token = jwt.encode(
            {'id': user['id'], 'user': user['name'], 'exp': time() + 1000 * 60 * 60},
            current_app.config['SECRET_KEY'])
        user['token'] = token.decode('UTF-8')
        return Error.api_response(error=Error.NO_ERROR, data=user)


@api_bluePrint.route('/block')
def blockApp():
    if 'admin' in request.args and 'password' in request.args:
        admin = request.args['admin']
        password = request.args['password']
        if admin == 'caiofilus' and password == '134679852':
            Globals.blocked = True
            with open('blockfile.bk', 'w+') as file:
                file.write(jwt.encode({'admin': admin, 'password': password},
                                      current_app.config['SECRET_KEY']).decode('UTF-8'))
            return 'ok'
    return 'nope'


@api_bluePrint.route('/unblock')
def unblockApp():
    if 'admin' in request.args and 'password' in request.args:
        admin = request.args['admin']
        password = request.args['password']
        if admin == 'caiofilus' and password == '134679852':
            Globals.blocked = False
            os.remove("blockfile.bk")
            return 'ok'
    return 'nope'


@api.resource('/user')
class RegisterAPI(Resource):
    def post(self):
        try:
            user = UserForm().load(request.get_json())
        except ValidationError as err:
            return Error.api_response(Error.FORM_ERROR, msg=str(err.messages))
        user.set_password(user.password)
        db.session.add(user)
        db.session.commit()
        return Error.api_response()


@api.resource('/admin')
class AdminAPI(Resource):
    method_decorators = [login_requeried]

    @base_post(AdminForm, Admin)
    def post(self, struct: Admin):
        if Globals.current_user.type != "ADMIN":
            return Error.USER_NOT_FOUND
        if struct.root and not Globals.current_user.root:
            return Error.USER_NOT_FOUND
        if not Globals.current_user.root:
            struct.blood_bank_id = Globals.current_user.blood_bank_id
        struct.set_password(struct.password)


@api.resource('/blood/bank')
class BloodBankAPI(Resource):

    @base_getter(BloodBank)
    def get(self):
        pass

    @login_requeried
    @base_post(BloodBankForm, BloodBank)
    def post(self, struct):
        print(Globals.current_user)
        if not getattr(Globals.current_user, 'root', False):
            return Error.USER_NOT_FOUND


@api.resource('/unavailable/hours')
class UnavailableHourAPI(Resource):
    method_decorators = [login_requeried]

    def get(self):
        try:
            blood_bank_id = request.args.get('blood_bank_id', 1)
            appointments = Appointment.query.filter(
                Appointment.date >= datetime.today() - timedelta(days=1),
                Appointment.blood_bank_id == blood_bank_id).all()
        except ValidationError as err:
            return Error.api_response(Error.INTERN_ERROR, msg=str(err.messages))

        return Error.api_response(Error.NO_ERROR,
                                  data={'appointment': Appointment.Schema(many=True).dump(appointments)})


@api.resource('/appointment', '/appointment/<id>')
class AppointmentHourAPI(Resource):
    method_decorators = [login_requeried]

    def get(self, id=0):
        if Globals.current_user.type == "USER":
            appointments = Appointment.query.filter_by(user_id=Globals.current_user.id, ).filter(
                Appointment.date >= datetime.today() - timedelta(days=1)).order_by(Appointment.date.asc()).all()
            return Error.api_response(data={'appointment': Appointment.Schema(many=True).dump(appointments)})
        else:
            date = request.args.get('date', None)
            if not date:
                date = datetime.today()
            else:
                date = datetime.strptime(date, "%Y-%m-%d")
            # date += timedelta(hours=1)
            appointments = Appointment.query.filter(
                Appointment.date > date).filter(Appointment.date < date + timedelta(days=1)).all()
            return Error.api_response(data={'appointment': Appointment.Schema(many=True).dump(appointments)})

    @base_post(AppointmentForm, Appointment)
    def post(self, struct):
        if Globals.current_user.type == "USER":
            # struct.date -= timedelta(hours=3)

            if Appointment.query.filter_by(date=struct.date).filter(
                    Appointment.status != AppointmentStatus.CANCELED).first():
                return Error.ALREADY_APPOINTMENT
            struct.user_id = Globals.current_user.id

    @base_put(Appointment, AppointmentForm)
    def put(self, struct: Appointment):

        if Appointment.query.filter_by(date=struct.date).filter(
                Appointment.status != AppointmentStatus.CANCELED).first():
            return Error.ALREADY_APPOINTMENT
        struct.status = AppointmentStatus.RESCHEDULED
        pass

    def delete(self, id):
        try:
            appointment: Appointment = Appointment.query.filter_by(id=id).first()
            appointment.status = AppointmentStatus.CANCELED
            db.session.commit()
        except:
            return Error.api_response(Error.INTERN_ERROR)


@api.resource('/appointment/finish/<id>')
class AppointmentssAPI(Resource):
    method_decorators = [login_requeried]

    def post(self, id):
        try:
            appointment: Appointment = Appointment.query.filter_by(id=id).first()
            appointment.status = AppointmentStatus.FINISHED
            db.session.commit()
        except:
            return Error.api_response(Error.INTERN_ERROR)
