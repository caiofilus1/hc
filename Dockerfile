#docker build -t hc
#docker run -p 5001:80 -d --restart always hc

FROM python:3.6.9


RUN pip install Flask flask-marshmallow Flask-RESTful Flask-SQLAlchemy
RUN pip install gunicorn

CMD  git clone  https://gitlab+deploy-token-174808:N67sky6CFcQeyfXhsowN@gitlab.com/caiofilus1/hc.git --depth=1 &&\
     cd hc &&\
     pip install -r requirements.txt &&\
     gunicorn -b 0.0.0.0:80 main:app