from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy

marshmallow: Marshmallow = Marshmallow()
db: SQLAlchemy = SQLAlchemy()
current_user = None
blocked = False


def init(app):
    db.init_app(app)
    from Database import Models
    db.create_all()

    marshmallow.init_app(app)
