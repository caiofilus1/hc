import enum

from sqlalchemy import Column, Integer, Float, Text, String, ForeignKey, DateTime, Boolean
from sqlalchemy.orm import relationship
from werkzeug.security import generate_password_hash, check_password_hash

from Database import EnumSmart
from Globals import db
from Lib.Wraps import Schema


class Gender(enum.Enum):
    MALE = 'MALE'
    FEMALE = 'FEMALE'
    OTHER = 'OTHER'


@Schema()
class BloodBank(db.Model):
    id = Column(Integer, primary_key=True)
    name = Column(String(40))
    city = Column(String(40))
    neighborhood = Column(String(40))
    street = Column(String(40))
    number = Column(Integer)
    img = Column(String(400))


@Schema()
class UserInterface(db.Model):
    id = Column(Integer, primary_key=True)
    name = Column(String(40))
    password = Column(Text)
    email = Column(String(40))

    type = Column(String(20))
    __mapper_args__ = {
        'polymorphic_on': type,
        'polymorphic_identity': 'USER_INTERFACE',
        'with_polymorphic': '*'
    }

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)


@Schema()
class User(UserInterface):
    phone = Column(String(40))
    age = Column(Integer)
    gender = Column(EnumSmart(Gender))
    weight = Column(Float)
    __mapper_args__ = {
        'polymorphic_identity': 'USER'
    }


@Schema()
class Admin(UserInterface):
    blood_bank_id = Column(Integer, ForeignKey('blood_bank.id'))
    root = Column(Boolean, default=False)
    __mapper_args__ = {
        'polymorphic_identity': 'ADMIN'
    }


class AppointmentStatus(enum.Enum):
    CANCELED = 'CANCELED'
    SCHEDULED = 'SCHEDULED'
    RESCHEDULED = 'RESCHEDULED'
    FINISHED = 'FINISHED'


@Schema(nested=['user', 'blood_bank'])
class Appointment(db.Model):
    id = Column(Integer, primary_key=True)
    date = Column(DateTime)
    status = Column(EnumSmart(AppointmentStatus), default=AppointmentStatus.SCHEDULED)
    blood_bank_id = Column(Integer, ForeignKey('blood_bank.id'))
    blood_bank = relationship(BloodBank, uselist=False)

    user_id = Column(Integer, ForeignKey('user_interface.id'), nullable=False)
    user = relationship(User, uselist=False)
