from typing import List, Text

from flask import json
from sqlalchemy import TypeDecorator, VARCHAR, String
from sqlalchemy.exc import DontWrapMixin


class IntList(TypeDecorator):
    impl = String(100)

    def process_bind_param(self, value: List[int], dialect):
        return json.dumps(value)

    def process_result_value(self, value: str, dialect):
        return json.loads(value)


class InvalidEnumException(Exception, DontWrapMixin):
    '''
    Classe de Exeção sugerida pela lib SQLalchemy
    '''
    pass


class EnumSmart(TypeDecorator):
    impl = String(50)

    def __init__(self, enumtype, by_value=False, *args, **kwargs):
        TypeDecorator.__init__(self, *args, **kwargs)
        self._enum = enumtype
        self.by_value = by_value

    def process_bind_param(self, value, dialect):
        if isinstance(value, str):
            try:
                tester = self._enum[value]
            except:
                raise InvalidEnumException("Not found " + value + ' in ' + self._enum.__class__.__name__ + ' Enum')
            return value
        return value.name

    def process_result_value(self, value, dialect):
        try:
            return self._enum[value]
        except:
            raise InvalidEnumException("Not found " + value + ' in ' + self._enum.__class__.__name__ + ' Enum')

    def copy(self, **kw):
        return EnumSmart(self._enum, self.impl.length)
