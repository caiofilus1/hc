import os

import sys
import logging

logging.basicConfig(stream=sys.stderr)
path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, path)

from main import app as application
