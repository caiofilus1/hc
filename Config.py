import os

basedir = os.path.abspath(os.path.dirname(__file__))


def database_url(user, password, server, database, driver='mysql+pymysql'):
    return '{driver}://{user}:{password}@{server}/{database}'.format(user=user,
                                                                     password=password,
                                                                     server=server,
                                                                     database=database,
                                                                     driver=driver)


class ConfigRelease(object):
    APPLICATION_ROOT = '/muninn'
    SQLALCHEMY_DATABASE_URI = database_url(user='hc',
                                           password='HospitalDeClinicas#414',
                                           server='localhost:3306',
                                           database='HC')

    SQLALCHEMY_TRACK_MODIFICATIONS = False


class ConfigDebug(object):
    APPLICATION_ROOT = '/muninn'
    SQLALCHEMY_DATABASE_URI = database_url(user='hc',
                                           password='HospitalDeClinicas#414',
                                           server='helloiot.com.br',
                                           database='HC')

    SQLALCHEMY_TRACK_MODIFICATIONS = False


class ConfigDebugLocal(ConfigDebug):
    SQLALCHEMY_DATABASE_URI = os.environ.get('SQLALCHEMY_DATABASE_URI') or \
                              'sqlite:///' + os.path.join(basedir, 'hc.db')
