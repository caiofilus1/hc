from marshmallow import validate
from marshmallow.fields import String, Integer, Email, Float, DateTime
from marshmallow_enum import EnumField

from Database.Models import User, Gender, Appointment, Admin, BloodBank
from Globals import marshmallow
from Lib.Wraps import ModelSchema


class LoginForm(marshmallow.Schema):
    email = String(required=True, validate=validate.Length(min=1, max=30))
    password = String(required=True)


class UserForm(ModelSchema):
    class Meta:
        model = User

    email = Email(required=True)
    name = String(required=True, validate=validate.Length(min=8, max=30))
    password = String(required=True, validate=validate.Length(min=6, max=30))
    phone = String(required=True, validate=validate.Length(min=6, max=30))
    age = Integer(required=True, validate=validate.Range(min=16, max=100))
    gender = EnumField(Gender, required=True)
    weight = Float(required=True)


class AdminForm(ModelSchema):
    class Meta:
        model = Admin

    email = Email(required=True)
    name = String(required=True, validate=validate.Length(min=8, max=30))
    password = String(required=True, validate=validate.Length(min=6, max=30))
    blood_bank_id = Integer(default=1)


class AppointmentForm(ModelSchema):
    class Meta:
        model = Appointment

    date = DateTime(required=True)
    blood_bank_id = Integer(required=True)


class BloodBankForm(ModelSchema):
    class Meta:
        model = BloodBank

    name = String(required=True)
    city = String(required=True)
    neighborhood = String(required=True)
    street = String(required=True)
    number = String(required=True)
    img = String(required=True)
