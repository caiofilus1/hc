import os
import traceback

from flask import Flask
from werkzeug.exceptions import NotFound

import Error
import Globals
from Config import ConfigDebug, ConfigDebugLocal, ConfigRelease
from Error import Error

app = Flask(__name__)

app.config.from_object(ConfigDebug()) if __name__ == '__main__' else app.config.from_object(ConfigRelease())

with app.app_context():
    Globals.init(app)

from Api import api_bluePrint
from Globals import db
from Database.Models import Admin

app.register_blueprint(api_bluePrint)


class BlockedError(Exception):
    error = Error.BLOCKED


@app.before_request
def before():
    if Globals.blocked:
        raise BlockedError


@app.errorhandler(Exception)
def handle_bad_request(e: Exception):
    if isinstance(e, NotFound):
        return '', 404
    elif isinstance(e, BlockedError):
        return Error.api_response(e.error)
    print(traceback.format_exc())
    print(e)
    return Error.api_response(Error.INTERN_ERROR)


@app.before_first_request
def handle_first_request():
    admins = Admin.query.all()
    print(admins)
    if len(admins) == 0:
        admin = Admin()
        admin.set_password('hsptldclncs')
        admin.name = "Usuário Root"
        admin.email = "root@email.com"
        admin.root = True
        db.session.add(admin)
        db.session.commit()


if __name__ == '__main__':
    app.config['SECRET_KEY'] = 'glauber'
    app.run(host='127.0.0.1', port=80, debug=True)
else:
    app.config['SECRET_KEY'] = os.urandom(12)
