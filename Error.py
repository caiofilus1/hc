import enum

from flask import make_response, jsonify


class Error(enum.Enum):
    NO_ERROR = {'id': 0, 'msg': ('',)}
    INTERN_ERROR = {'id': 1, 'msg': ('Erro Interno, Favor Contatar a Assistencia',)}
    FORM_ERROR = {'id': 2, 'msg': ('Erro no Formulário',)}
    USER_NOT_FOUND = {'id': 20, 'msg': ('Usuário não encontrado',)}
    ALREADY_EXIST = {'id': 21, 'msg': ("Usuario já existe",)}
    USER_OR_PASS_INCORRECT = {'id': 22, 'msg': ("Login ou senha não existem",)}
    USER_NOT_LOGGED = {'id': 23, 'msg': ("Usuario não logado",)}
    MISSING_TOKEM = {'id': 24, 'msg': ("Token faltando",)}
    INVALID_TOKEN = {'id': 25, 'msg': ("Token inválido",)}
    BLOCKED = {'id': 25, 'msg': ("Servidor bloqueado, favor consultar o Desenvolvedor",)}
    ALREADY_APPOINTMENT = {'id': 30, 'msg': ("Horário já foi reservado, Favor escolha outro",)}

    @staticmethod
    def api_response(error=NO_ERROR, data={}, msg=None, isSocket: bool = False):
        data['msg'] = msg if msg else (error.value['msg'][0] if isinstance(error, Error) else error['msg'][0])
        data['status'] = error.value['id'] if isinstance(error, Error) else error['id']
        http_code = 200 if data['status'] == 0 else 500
        if not isSocket:
            return make_response(jsonify(data), http_code)
        else:
            return data
