module.exports = {
    devServer: {
        proxy: {
            "^/api": {
                target: "http://localhost:80",
                // target: "http://helloiot.com.br",
                ws: true,
                changeOrigin: true,

            }
        },
    },
    css: {
        // Enable CSS source maps.
        sourceMap: true
    },
    // publicPath: "/HC/",
};
