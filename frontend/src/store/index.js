import Vue from 'vue';
import Vuex from 'vuex';

import {snackbar} from './snackbar';
import {user} from './user';
import {bloodBank} from './bloodBank';

Vue.use(Vuex);

export const store = new Vuex.Store({
    modules: {
        snackbar,
        user,
        bloodBank,
    }
});
