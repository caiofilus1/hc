const USER_KEY = "HC_USER";

let storage = localStorage.getItem(USER_KEY);
let defaultJson = storage === 'undefined' ? undefined : JSON.parse(localStorage.getItem(USER_KEY));

export const state = () => ({
    user: defaultJson,
});

export const mutations = {
    setUser(state, user) {
        state.user = user || undefined;
        localStorage.setItem(USER_KEY, JSON.stringify(state.user));
    },
    removeUser(state) {
        localStorage.removeItem(USER_KEY);
        state.user = undefined;
    }
};

export const user = {
    namespaced: true,
    state,
    mutations
};
