export const state = () => ({
    show: false,
    text: "",
    color: "",
});

export const mutations = {
    error(state, text) {
        state.show = true;
        state.text = text;
        state.color = 'error';
    },
    info(state, text) {
        state.show = true;
        state.text = text;
        state.color = 'info';
    },
    success(state, text) {
        state.show = true;
        state.text = text;
        state.color = 'success';
    },
    dialog(state, {text, color = 'info'}) {
        state.show = true;
        state.text = text;
        state.color = color;
    },
    close(state) {
        state.show = false;
    }
};

export const snackbar = {
    namespaced: true,
    state,
    mutations
};
