const BLOODBANK_KEY = "BLOODBANKS";

let storage = localStorage.getItem(BLOODBANK_KEY);
let defaultJson = storage === 'undefined' ? undefined : JSON.parse(localStorage.getItem(BLOODBANK_KEY));


export const state = () => ({
    bloodBanks: defaultJson,
});

export const mutations = {
    setBloodBanks(state, bloodBanks) {
        console.log(bloodBanks);
        state.bloodBanks = bloodBanks;
        localStorage.setItem(BLOODBANK_KEY, JSON.stringify(state.bloodBanks));
    },
};

export const bloodBank = {
    namespaced: true,
    state,
    mutations
};
