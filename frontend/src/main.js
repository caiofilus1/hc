import Vue from 'vue'
import App from './App.vue'
import axios from './plugins/axios'
import vuetify from './plugins/vuetify';
import router from "./plugins/vueRouter";
import VueMoment from 'vue-moment'
import moment from 'moment-timezone'
import {store} from './store/index'

Vue.config.productionTip = false;
Vue.use(axios);
Vue.use(VueMoment, {
    moment,
});

Vue.prototype.$http = axios;
Vue.prototype.moment = moment;

document.title = "HC-Doação de Sangue";

new Vue({
    vuetify,
    router,
    store,
    moment,
    render: h => h(App)
}).$mount('#app');
