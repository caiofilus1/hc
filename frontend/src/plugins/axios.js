import axios from 'axios'
import router from 'vue-router'
import {store} from '@/store/index'
// import {userService} from '@/core/API/auth/userCrud'

var HOST = "";

const URL = {
    LOGIN: '/api/login',

    USER: '/api/user',
    BLOOD_BANK: '/api/blood/bank',
    ADMIN: '/api/admin',
    APPOINTMENT: '/api/appointment',
    FINISH_APPOINTMENT: '/api/appointment/finish',
    UNAVAILABLE: '/api/unavailable/hours',

};


axios.interceptors.request.use(function (config) {
    if (config.url.startsWith('http')) {
        return config
    }
    let user = store.state.user.user;
    if (user) {
        config.headers.token = user.token;
    }
    if (config.method === "put") {
        config.url += '/' + config.data['id'];
    }
    if (!config.url.includes(HOST)) {
        config.url = HOST + config.url;
    }
    return config;
}, function (error) {
    return Promise.reject(error);
});

axios.interceptors.response.use(function (response) {
    if (response.config.url.startsWith(HOST + URL.LOGIN)) {
        store.commit('user/setUser', response.data);
        // localStorage.setItem('user', JSON.stringify(response.data));
    }
    return response;
}, async function (response) {
    let data = response.response;
    if (data && data.status === 500) {
        const error = data.data.msg ? data.data.msg : data.statusText;
        if (data.data.status === 25 || data.data.status === 23 || data.data.status === 24) {
            try {
                let user = store.state.user.user;
                let responseLogin = await axios.post(URL.LOGIN, {email: user.email, password: user.password});
                if (responseLogin) {
                    var res = await axios.request(data.config);
                    this.resolve(res)
                } else {
                    router.next('/login');
                    return Promise.reject(error);
                }
            } catch (e) {
                try {
                    router.next('/login')
                } catch (e) {
                    router
                }
                return Promise.reject(error);
            }
        } else if (data.data.status === 20) {
            store.commit('user/removeUser');
        }
        return Promise.reject(error);
    } else if (data.config.url.startsWith(URL.LOGIN)) {
        store.commit('user/setUser', data.data);
        this.resolve(data)
    } else if (!data) {
        return Promise.reject(response);
    }
    return Promise.reject(data.statusText);
});
export {axios, URL};
export default axios;