import Vue from 'vue'
import Router from 'vue-router'
import Login from "../core/Login/Login";
import Main from "../core/Main/Main";
import User from "../core/Main/User/User";
import Admin from "../core/Main/Admin/Admin";
import Root from "../core/Main/Admin/Root";
import {store} from '@/store/index'

Vue.use(Router);


const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/login',
            name: 'login',
            component: Login
        }, {
            path: '/',
            name: 'Main',
            component: Main,
            children: [
                {
                    path: '/user',
                    name: 'User',
                    component: User,
                }, {
                    path: '/admin',
                    name: 'Admin',
                    component: Admin,
                }, {
                    path: '/root',
                    name: 'Root',
                    component: Root,
                }
            ]
        },

    ]
});


router.beforeEach((to, from, next) => {
    // redirect to login page if not logged in and trying to access a restricted page
    const publicPages = ['/login'];
    const authRequired = !publicPages.includes(to.path);
    const loggedIn = store.state.user.user;

    if (authRequired && !loggedIn) {
        return next('/login');
    }
    next();
});

export default router;
