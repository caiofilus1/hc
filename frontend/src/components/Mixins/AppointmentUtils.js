const AppointmentUtils = {
    data() {
        return {
            APPOINTMENT_STATUS: {
                CANCELED: 'CANCELED',
                SCHEDULED: 'SCHEDULED',
                RESCHEDULED: 'RESCHEDULED',
                FINISHED: 'FINISHED'
            }
        }
    }

};

export default AppointmentUtils;